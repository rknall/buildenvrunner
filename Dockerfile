FROM gitlab/gitlab-runner:latest

RUN apt-get update -y && \
    apt-get install -y gpg && \
    mkdir update && \
    cd update && \
    wget https://apt.kitware.com/keys/kitware-archive-latest.asc && \
    apt-key add kitware-archive-latest.asc && \
    echo 'deb https://apt.kitware.com/ubuntu/ focal main' > /etc/apt/sources.list.d/kitware.list && \
    apt-get update -y && \
    apt-get install -y cmake build-essential make && \
    cmake --version

